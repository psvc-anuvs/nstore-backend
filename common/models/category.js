module.exports = function (Category) {

	var getNextCount = "function(name){return getNextSequence(name); }"
	// TODO : Check how it behaves when concurrent user creates
	Category.observe('before save', function addIdAndupdateTimestamp(ctx, next) {
		Category.app.datasources.mongodb.connector.db.eval(
			getNextCount, ['cat_id'], function (err, result) {
				if (err) {
					console.log("There was an error calling dbFunc", err);
					return;
				}

				if (ctx.instance) {
					ctx.instance.lastupdated = new Date();
					ctx.instance.cat_id = result;
				} else {
					ctx.data.lastupdated = new Date();
				}
				next();

			});
	});

};
